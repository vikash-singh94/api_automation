/**
 * 
 */
package com.webservices.bean;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.Randomizer;

/**
 * @author Vikash.Singh
 *
 */
public class UserBean extends BaseDataBean{
	
	@Randomizer
	private String userFirstName;

	@Randomizer
	private String userLastName;
	
	@Randomizer
	private static String userName;

	@Randomizer
	private String userPassword;


	@Randomizer(suffix = "@infoneers.com")
	private String email;


	public String getUserFirstName() {
		return userFirstName;
	}


	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}


	public String getUserLastName() {
		return userLastName;
	}


	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}


	public String getUserName() {
		return userName;
	}


	public static void setUserName(String userName) {
		UserBean.userName = userName;
	}


	public String getUserPassword() {
		return userPassword;
	}


	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	
	
	

}
