/**
 * 
 */
package com.webservices.tests;

import org.hamcrest.Matchers;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.qmetry.qaf.automation.core.ConfigurationManager;
//import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.qmetry.qaf.automation.ws.rest.RestWSTestCase;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.webservices.bean.UserBean;

import sun.misc.BASE64Encoder;


/**
 * @author Vikash.Singh
 *
 */
public class MakeOrder extends RestWSTestCase {
	
	String productId = "808a2de1-1aaa-4c25-a9b9-6612e8f29a38";
	String productName = "Crossed";
	String productDesc = "A mature sock, crossed, with an air of nonchalance.";
	Double price = 17.32;	
	@Test(priority = 1)
	public void registerUser() {
		UserBean userBean= new UserBean();

		// fill bean with Random data and pass this to post request
		userBean.fillRandomData();
		
		Reporter.log("UserName :"+userBean.getUserName(), true);
		Reporter.log("UserPassword :"+userBean.getUserPassword(), true);
		
		//Storing username and password for login
		ConfigurationManager.getBundle().setProperty("username", userBean.getUserName());
		ConfigurationManager.getBundle().setProperty("password", userBean.getUserPassword());
		
		// Post request to Register new user
		getWebResource("/register").header("Content-Type", "application/json").post(new Gson().toJson(userBean));
		
		
		JSONObject JSONObj=new JSONObject(getResponse().getMessageBody());
		
		// Response validation
		Validator.verifyThat("Registration Done Successfully", JSONObj.getString("id").length(), Matchers.greaterThan(10));
		
		//Storing Product id to compare
		String id = (String) JSONObj.get("id");
		ConfigurationManager.getBundle().setProperty("id", id);
	}
	@Test(priority = 2)
	public void userLogin() {
		// Fetch value for Username and Password stored in Register test.
		String username  = (String) ConfigurationManager.getBundle().getProperty("username");
		String password  = (String) ConfigurationManager.getBundle().getProperty("password");
		
		
		String url = (String) ConfigurationManager.getBundle().getProperty("env.baseurl");
		String loginUrl=url+"login";
		System.out.println("URL is :"+loginUrl);
        
		String authString = username + ":" + password;
        String authStringEnc = new BASE64Encoder().encode(authString.getBytes());
        Reporter.log("Base64 encoded auth string: " + authStringEnc, true);
        
        Client restClient = Client.create();
        WebResource webResource = restClient.resource(loginUrl);
        ClientResponse response = webResource.accept("application/json") .header("Authorization", "Basic " + authStringEnc).get(ClientResponse.class);
        
        if(response.getStatus() != 200){
            System.err.println("Not able to connect to the server");
        }
        
        String responseOutput = response.getEntity(String.class);
        System.out.println("response: "+responseOutput);
	}
	
	@Test(priority = 3)
	public void addInCart() {
		// Creating a JSONObject to store Product Id
		JSONObject jsonObject= new JSONObject();
		jsonObject.put("id", productId);
		
		// Post ADD CART request
		getWebResource("/cart").header("Content-Type", "application/json").post(jsonObject.toString());
		
		// Get Client Response and check for Status
		ClientResponse clientResponse=getWebResource("/cart").header("Content-Type", "application/json").get(ClientResponse.class);
		System.out.println("Client Response is : "+clientResponse.getStatus());	

		if(Integer.toString(clientResponse.getStatus()).equals("200 OK")){
			Validator.verifyThat("Item has been added in cart successfully.", Integer.toString(clientResponse.getStatus()), Matchers.equalTo("200 OK"));
		}
		else {
			Validator.verifyThat("Unable to add item in cart.", Integer.toString(clientResponse.getStatus()), Matchers.not("200 OK"));
		}
		
	}
	@Test(priority = 4)
	public void addCardDetails() {
		// Add the card details to json object
		JSONObject cardObj= new JSONObject();
		cardObj.put("longNum", "4477474202648005");
		cardObj.put("expiry", "03/22");
		cardObj.put("cvv", "123");
		cardObj.put("userID", ConfigurationManager.getBundle().getProperty("id"));
		
		// Post request for Cards URI
		getWebResource("/cards").header("Content-Type", "application/json").post(cardObj.toString());
		
		// Cast response to json and verify is the response is correct
		JSONObject JSONObj=new JSONObject(getResponse().getMessageBody());
		Validator.verifyThat("Card details added successfully.", JSONObj.getString("id").length(), Matchers.greaterThan(10));
	}
	@Test(priority = 5)
	public void addAddress() {
		// Add the Address parameters to json object
		JSONObject addressObj= new JSONObject();
		addressObj.put("street", "XYZ ROAD");
		addressObj.put("number", "123");
		addressObj.put("country", "India");
		addressObj.put("city", "PUNE");
		addressObj.put("postcode", "221109");
		addressObj.put("userID", ConfigurationManager.getBundle().getProperty("id"));
		
		// Post request to Addresses URI
		getWebResource("/addresses").header("Content-Type", "application/json").post(addressObj.toString());
		
		
		JSONObject JSONObj=new JSONObject(getResponse().getMessageBody());
		Validator.verifyThat("Address details has been added successfully.", JSONObj.getString("id").length(), Matchers.greaterThan(10));
	}
	@Test(priority = 6)
	public void placeAnOrder() {
		// Post request to Order
		getWebResource("/orders").header("Content-Type", "application/json").post();
	}
	
	@Test(priority = 7)
	public void verifyOrder() {
		// Get request to Orders URI
		ClientResponse clientResponse=getWebResource("/orders").header("Content-Type", "application/json").get(ClientResponse.class);
		System.out.println("Client response code while placing an order:"+clientResponse.getStatus());	
		
		Validator.verifyThat("Order has been placed successfully.",  Integer.toString(clientResponse.getStatus()), Matchers.equalTo("201"));		
	}

}
